#include <iostream>
#include <windows.h>

int main()
{
    SYSTEMTIME Day;
    GetSystemTime(&Day);

    int RowN;
    int Sum = 0;

    const int size = 7;

    int array[size][size];

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j];
        }
        std::cout << "\n";
    }
        
        RowN = (Day.wDay % size);

    std::cout << "\n";
    for (int j = 0; j < size; j++)
    {
        Sum = Sum + array[RowN][j];
    }      

    std::cout << "The sum of the elements in line number _" << RowN << "_ is: " << Sum << "\n";

    return 0;
}
